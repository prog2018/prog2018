## Kaip tvarkytis su šitu šūdu?

1. Pirmiausia reikia atsisiųsti git https://git-scm.com/downloads
2. Atsidarai konsolę win+r rašai "CMD";
3. Vedi šitą kodą (git clone https://povilasleka@bitbucket.org/prog2018/prog2018.git) be skliaustų.
4. Rašai konsolėje 'cd prog2018' be kabučių ir spaudi enter.
5. Pasuki tėvo bybi 90 laipsnių.
6. Į konsolę rašai 'git init' be kabučių.
7. Dabar tu toj papkėj laikai visus savo kodus tos programos ir kai nori įkelti į serverį rašai 'git add .' , 'git commit -am "atnaujinimas"' , 'git push';
8. Ir turėtų viskas veikt.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
